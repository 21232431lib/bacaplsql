--++++ 13/07/2020
-- Diapositivas 1_3

DECLARE
    v_first_name  VARCHAR2(25);
    v_last_name   VARCHAR2(25);
BEGIN
    SELECT
        first_name,
        last_name
    INTO
        v_first_name,
        v_last_name
    FROM
        employees
    WHERE
        last_name = 'Abel';

    dbms_output.put_line('The employee of the month is: '
                         || v_first_name
                         || ' '
                         || v_last_name
                         || '.');

EXCEPTION
    WHEN too_many_rows THEN
        dbms_output.put_line('Your select statement retrieved multiple rows. Consider using a cursor or changing the search criteria.');
END;

DECLARE
    v_deptno copy_emp.department_id%TYPE := 50;
BEGIN
    DELETE FROM copy_emp
    WHERE
        department_id = v_deptno;

    dbms_output.put_line(SQL%rowcount || ' rows deleted.');
END;