---++++ 14 julio
---SESION 2
--+++++diap 5_1

--++Ejemplo1
DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name
    FROM
        departments;

    v_department_id    departments.department_id%TYPE;
    v_department_name  departments.department_name%TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO
            v_department_id,
            v_department_name;
        EXIT WHEN cur_depts%notfound;
        dbms_output.put_line(v_department_id
                             || ' '
                             || v_department_name);
    END LOOP;

    CLOSE cur_depts;
END;


--++Ejemplo 2

DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname);
    END LOOP;

END;

---++

DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name,
        salary
    FROM
        employees
    WHERE
        department_id = 30;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
    v_sal    employees.salary%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname,
            v_sal;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname
                             || v_sal);
    END LOOP;

    CLOSE cur_emps;
END;


--+++++
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 10;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname);
    END LOOP;

END;

--+++

DECLARE
    CURSOR cur_emps IS
    SELECT
        *
    FROM
        employees
    WHERE
        department_id = 30;

    v_emp_record cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_record.salary
                             || ' - '
                             || v_emp_record.last_name);
    END LOOP;

    CLOSE cur_emps;
END;

---+++

DECLARE
    CURSOR cur_emps_dept IS
    SELECT
        first_name,
        last_name,
        department_name
    FROM
        employees    e,
        departments  d
    WHERE
        e.department_id = d.department_id;

    v_emp_dept_record cur_emps_dept%rowtype;
BEGIN
    OPEN cur_emps_dept;
    LOOP
        FETCH cur_emps_dept INTO v_emp_dept_record;
        EXIT WHEN cur_emps_dept%notfound;
        dbms_output.put_line(v_emp_dept_record.first_name
                             || ' � '
                             || v_emp_dept_record.last_name
                             || ' � '
                             || v_emp_dept_record.department_name);

    END LOOP;

    CLOSE cur_emps_dept;
END;

--+++
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees;

    v_emp_record cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%rowcount > 10 OR cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;

    CLOSE cur_emps;
END;


--+++FOR


DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

BEGIN
    FOR v_emp_record IN cur_emps LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;

--++SUBQUERY
BEGIN
    FOR v_emp_record IN (
        SELECT
            employee_id,
            last_name
        FROM
            employees
        WHERE
            department_id = 50
    ) LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;

--++

DECLARE
    CURSOR cur_country (
        p_region_id NUMBER
    ) IS
    SELECT
        country_id,
        country_name
    FROM
        countries
    WHERE
        region_id = p_region_id;

    v_country_record cur_country%rowtype;
BEGIN
    OPEN cur_country(1);
    LOOP
        FETCH cur_country INTO v_country_record;
        EXIT WHEN cur_country%notfound;
        dbms_output.put_line(v_country_record.country_id
                             || ' '
                             || v_country_record.country_name);
    END LOOP;

    CLOSE cur_country;
END;

--+++
DECLARE
    CURSOR cur_countries (
        p_region_id   NUMBER,
        p_country_id  CHAR
    ) IS
    SELECT
        country_id,
        country_name
    FROM
        countries
    WHERE
        region_id = p_region_id
        OR country_id = 'BR';

BEGIN
    FOR v_country_record IN cur_countries(145, 'BR') LOOP
        dbms_output.put_line(v_country_record.country_id
                             || '----'
                             || v_country_record.country_name);
    END LOOP;
END;

--++
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        salary
    FROM
        employees
    WHERE
        salary <= 20000
    FOR UPDATE NOWAIT;

    v_emp_rec cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_rec;
        EXIT WHEN cur_emps%notfound;
        UPDATE employees
        SET
            salary = v_emp_rec.salary * 1.1
        WHERE
            CURRENT OF cur_emps;

    END LOOP;

    CLOSE cur_emps;
END;



