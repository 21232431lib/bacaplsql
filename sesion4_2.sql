--++ Sesion 4_2

CREATE OR REPLACE PACKAGE global_consts IS
    mile_to_kilo CONSTANT NUMBER := 1.6093;
    kilo_to_mile CONSTANT NUMBER := 0.6214;
    yard_to_meter CONSTANT NUMBER := 0.9144;
    meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;

GRANT EXECUTE ON global_consts TO PUBLIC;

DECLARE
    distance_in_miles  NUMBER(5) := 5000;
    distance_in_kilo   NUMBER(6, 2);
BEGIN
    distance_in_kilo := distance_in_miles * global_consts.mile_to_kilo;
    dbms_output.put_line(distance_in_kilo);
END;

CREATE TABLE excep_test (
    number_col NUMBER(3)
);

BEGIN
    INSERT INTO excep_test ( number_col ) VALUES ( 999 );

EXCEPTION
    WHEN our_exceptions.e_value_too_large THEN
        dbms_output.put_line('Value too big for column data type');
END;

CREATE OR REPLACE PACKAGE taxes_pkg IS
    FUNCTION tax (
        p_value IN NUMBER
    ) RETURN NUMBER;

END taxes_pkg;

CREATE OR REPLACE PACKAGE BODY taxes_pkg IS

    FUNCTION tax (
        p_value IN NUMBER
    ) RETURN NUMBER IS
        v_rate NUMBER := 0.08;
    BEGIN
        return(p_value * v_rate);
    END tax;

END taxes_pkg;

SELECT
    taxes_pkg.tax(salary),
    salary,
    last_name
FROM
    employees;

CREATE OR REPLACE PROCEDURE sel_one_emp (
    p_emp_id  IN   employees.employee_id%TYPE,
    p_emprec  OUT  employees%rowtype
) IS
BEGIN
    SELECT
        *
    INTO p_emprec
    FROM
        employees
    WHERE
        employee_id = p_emp_id;

END sel_one_emp;

DECLARE
    v_emprec employees%rowtype;
BEGIN
    sel_one_emp(100, v_emprec);
    dbms_output.put_line(v_emprec.last_name);
END;

CREATE OR REPLACE PACKAGE curs_pkg IS
    CURSOR emp_curs IS
    SELECT
        employee_id
    FROM
        employees
    ORDER BY
        employee_id;

    PROCEDURE open_curs;

    FUNCTION fetch_n_rows (
        n NUMBER := 1
    ) RETURN BOOLEAN;

    PROCEDURE close_curs;

END curs_pkg;

CREATE OR REPLACE PACKAGE BODY curs_pkg IS

    PROCEDURE open_curs IS
    BEGIN
        IF NOT emp_curs%isopen THEN
            OPEN emp_curs;
        END IF;
    END open_curs;

    FUNCTION fetch_n_rows (
        n NUMBER := 1
    ) RETURN BOOLEAN IS
        emp_id employees.employee_id%TYPE;
    BEGIN
        FOR count IN 1..n LOOP
            FETCH emp_curs INTO emp_id;
            EXIT WHEN emp_curs%notfound;
            dbms_output.put_line('Id: ' ||(emp_id));
        END LOOP;

        RETURN emp_curs%found;
    END fetch_n_rows;

    PROCEDURE close_curs IS
    BEGIN
        IF emp_curs%isopen THEN
            CLOSE emp_curs;
        END IF;
    END close_curs;

END curs_pkg;

DECLARE
    v_more_rows_exist BOOLEAN := true;
BEGIN
    curs_pkg.open_curs;
LOOP
v_more_rows_exist := curs_pkg.fetch_n_rows(3);
DBMS_OUTPUT.PUT_LINE('-------');
EXIT WHEN NOT v_more_rows_exist;
    END LOOP; 
    curs_pkg.close_curs;              
    END;
    
 DECLARE
    v_more_rows_exist BOOLEAN := TRUE;
    n NUMBER := 1;
BEGIN
    curs_pkg.open_curs; --1
    LOOP
        v_more_rows_exist := curs_pkg.fetch_n_rows(n); --2
        DBMS_OUTPUT.PUT_LINE('-------');
        n:=n+1;
        EXIT WHEN NOT v_more_rows_exist;
    END LOOP;
    curs_pkg.close_curs; --3
END;