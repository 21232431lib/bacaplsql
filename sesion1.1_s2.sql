--- DIAPOSITIVAS 1_3

BEGIN
    dbms_output.put_line('PL/SQL is easy!');
END;

DECLARE
    v_date DATE := sysdate;
BEGIN
    dbms_output.put_line(v_date);
END;

CREATE OR REPLACE PROCEDURE print_date IS
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate + 1, 'Mon DD, YYYY')
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;

BEGIN
    print_date;
END;

--++cREA UNA FUNCION QUE RECIBE UN VALOR Y DEVUELVE EL QUE SE GENERA DENTRO DE LA FUNCION
CREATE OR REPLACE FUNCTION tomorrow (
    p_today IN DATE
) RETURN DATE IS
    v_tomorrow DATE;
BEGIN
    SELECT
        p_today + 1
    INTO v_tomorrow
    FROM
        dual;

    RETURN v_tomorrow;
END;
--++VERIFICA EL FUNCIONAMIENTO DE LA FUNCION
SELECT
    tomorrow(sysdate) AS "Tomorrow's Date"
FROM
    dual;

---+++OTRA FORMA DE VERIFICAR 
BEGIN
    dbms_output.put_line(tomorrow(sysdate));
END;


--++ DIAPOSITIVAS 2_1

DECLARE
    v_counter INTEGER := 0;
BEGIN
    v_counter := v_counter + 1;
    dbms_output.put_line(v_counter);
END;

DECLARE
    v_myname VARCHAR2(20);
BEGIN
    dbms_output.put_line('My name is: ' || v_myname);
    v_myname := 'John';
    dbms_output.put_line('My name is: ' || v_myname);
END;

CREATE FUNCTION num_characters (
    p_string IN VARCHAR2
) RETURN INTEGER IS
    v_num_characters INTEGER;
BEGIN
    SELECT
        length(p_string)
    INTO v_num_characters
    FROM
        dual;

    RETURN v_num_characters;
END;

DECLARE
    v_length_of_string INTEGER;
BEGIN
    v_length_of_string := num_characters('Oracle Corporation');
    dbms_output.put_line(v_length_of_string);
END;

DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20) := 'Mike';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
    END;

    dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
    dbms_output.put_line('Child''s Name: ' || v_child_name);
END;

DECLARE
    v_first_name  VARCHAR2(20);
    v_last_name   VARCHAR2(20);
BEGIN
    BEGIN
        v_first_name := 'Carmen';
        v_last_name := 'Miranda';
        dbms_output.put_line(v_first_name
                             || ' '
                             || v_last_name);
    END;

    dbms_output.put_line(v_first_name
                         || ' '
                         || v_last_name);
END;


<<ops>>
DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name     VARCHAR2(20) := 'Mike';
        v_date_of_birth  DATE := '12-Diciembre-2002';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || ops.v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
    END;
END;


DECLARE
    v_father_name VARCHAR2(20):='Patrick';
    v_date_of_birth DATE:='20-Abr-1972';
BEGIN
   -- <<externo>> Podemos usar la etiqueta en el nivel que se desee, solo para ver el nivel de aacceso
    DECLARE
        v_child_name VARCHAR2(20):='Mike';
        v_date_of_birth DATE:='12-Dec-2002';
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Father''s Name: ' || v_father_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || externo.v_date_of_birth);
        DBMS_OUTPUT.PUT_LINE('Child''s Name: '